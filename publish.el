(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions

;; Disable Babel code evaluation.
(setq org-export-babel-evaluate nil)

;; Load style presets.
(load-file "styles.el")
    
;; Preparation functions
;; If used, these functions shall be executed before exporting a particular item
;; of the project alist.
(defun load-compas-latex-class (&optional project-plist)
  ;; LaTeX export engine requires class files to be located in the same folder
  ;; as the source file being processed. Although, we store our custom classes
  ;; in separate folders in the 'styles' directory. This preparation function
  ;; can be used to make symbolic links to the 'compas2021' class files in the
  ;; current working directory.
  (make-symbolic-link
   "styles/compas/compas2021.cls"
   "examples/compas2021.cls"
   t)
  (make-symbolic-link
   "styles/compas/compas.bst"
   "examples/compas.bst"
   t))

(defun load-europar-latex-class (&optional project-plist)
  ;; LaTeX export engine requires class files to be located in the same folder
  ;; as the source file being processed. Although, we store our custom classes
  ;; in separate folders in the 'styles' directory. This preparation function
  ;; can be used to make symbolic links to the 'llncs' class files in the
  ;; current working directory.
  (make-symbolic-link
   "styles/europar/llncs.cls"
   "examples/llncs.cls"
   t)
  (make-symbolic-link
   "styles/europar/splncs04.bst"
   "examples/splncs04.bst"
   t))

(defun load-siam-latex-class (&optional project-plist)
  ;; LaTeX export engine requires class files to be located in the same folder
  ;; as the source file being processed. Although, we store our custom classes
  ;; in separate folders in the 'styles' directory. This preparation function
  ;; can be used to make symbolic links to the 'siamart220329' class files in
  ;; the current working directory.
  (make-symbolic-link
   "styles/siam/siamart220329.cls"
   "examples/siamart220329.cls"
   t)
  (make-symbolic-link
   "styles/siam/siamplain.bst"
   "examples/siamplain.bst"
   t))

;; Completion functions
;; If used, these functions shall be executed after exporting a particular item
;; of the project alist.
(defun unload-compas-latex-class (&optional project-plist)
  ;; Clean the symbolic links created by the preparation function
  ;; `load-compas-latex-class`.
  (delete-file "examples/compas2021.cls")
  (delete-file "examples/compas.bst"))

(defun unload-europar-latex-class (&optional project-plist)
  ;; Clean the symbolic links created by the preparation function
  ;; `load-europar-latex-class`.
  (delete-file "examples/llncs.cls")
  (delete-file "examples/splncs04.bst"))

(defun unload-siam-latex-class (&optional project-plist)
  ;; Clean the symbolic links created by the preparation function
  ;; `load-siam-latex-class`.
  (delete-file "examples/siamart220329.cls")
  (delete-file "examples/siamplain.bst"))

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "index"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["index.org"]
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public")
       (list "compas"
             :base-directory "./examples"
             :base-extension "org"
             :exclude ".*"
             :include ["compas.org"]
             :preparation-function '(load-compas-latex-class)
             :completion-function '(unload-compas-latex-class)
             :publishing-function '(org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public/examples")
       (list "europar"
             :base-directory "./examples"
             :base-extension "org"
             :exclude ".*"
             :include ["europar.org"]
             :preparation-function '(load-europar-latex-class)
             :completion-function '(unload-europar-latex-class)
             :publishing-function '(org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public/examples")
       (list "siam"
             :base-directory "./examples"
             :base-extension "org"
             :exclude ".*"
             :include ["siam.org"]
             :preparation-function '(load-siam-latex-class)
             :completion-function '(unload-siam-latex-class)
             :publishing-function '(org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public/examples")
       (list "RR"
             :base-directory "./examples"
             :base-extension "org"
             :exclude ".*"
             :include ["RR.org"]
             :publishing-function '(org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public/examples")
       (list "guix"
             :base-directory "./examples"
             :base-extension "org"
             :exclude ".*"
             :include ["guix.org"]
             :preparation-function '(set-minted-options-for-dark-background)
             :completion-function '(unset-minted-options-for-dark-background)
             :publishing-function '(org-beamer-publish-to-pdf)
             :publishing-directory "./public/examples")
       (list "poster"
             :base-directory "./examples"
             :base-extension "tex"
             :exclude ".*"
             :include ["poster.tex"]
             :publishing-function '(latex-publish-to-pdf)
             :publishing-directory "./public/examples")
       (list "styles"
             :components '("index" "compas" "europar" "siam" "RR" "guix"
                           "poster"))))

(provide 'publish)
