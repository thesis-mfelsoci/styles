;; Styles presets, associated LaTeX package configurations, preparation,
;; completion and export functions

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("compas2021"
                 "\\documentclass{compas2021}
\\usepackage{hyperref}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("IEEEtran"
                 "\\documentclass{IEEEtran}
\\usepackage{hyperref}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("llncs"
                 "\\documentclass{llncs}
\\usepackage{hyperref, wrapfig}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("siamart220329"
                 "\\documentclass{siamart220329}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

;; Define a function for selective content export based on the backend.
(defun filter-export (backend)
  "Remove every headline with ':htmlonly:' tag from the tree when exporting to
LaTeX. BACKEND provides the export back-end being used."
  (if (org-export-derived-backend-p backend 'latex)
      (org-map-entries '(org-toggle-tag "noexport" "on") "+htmlonly")))

;; Add the selective export filter function as a hook.
(add-hook 'org-export-before-parsing-hook 'filter-export)

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Preserve user-defined labels during the export to PDF via LaTeX.
(setq org-latex-prefer-user-labels t)

;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process (list "latexmk --shell-escape -f -pdf %f"))

;; Do not prompt for code block evaluation.
(setq org-confirm-babel-evaluate nil)

;; Custom publishing function for LaTeX files. This is useful when we need to
;; publish a manually written LaTeX file to PDF (e. g. a poster).
(defun latex-publish-to-pdf (plist filename pub-dir)
  (let
      ;; Keep the initial directory.
      ((cwd default-directory))
    ;; Navigate to the directory of the LaTeX file to publish.
    (cd (file-name-directory filename))
    ;; Call the interal publishing function.
    (org-latex-compile filename)
    ;; Publish the PDF file to the destination set in the project's alist.
    (org-publish-attachment
     plist
     (concat (file-name-sans-extension filename) ".pdf")
     pub-dir)
    ;; Return to the initial directory.
    (cd cwd)))

;; Use a custom LaTeX publishing function which redirects the LaTeX error output
;; to the standard output so we can see the errors even in batch mode.
(defun org-latex-publish-to-pdf-verbose (plist filename pub-dir)
  (condition-case err
      (org-latex-publish-to-pdf
       plist filename pub-dir)
    ;; Handle Org errors at first.
    (user-error
     (message "%s" (error-message-string err)))
    ;; If the Org source is fine, handle LaTeX errors.
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (append-to-file (point-min) (point-max) "/dev/stdout")))))

(defun org-beamer-publish-to-pdf-verbose (plist filename pub-dir)
  (condition-case err
      (org-beamer-publish-to-pdf
       plist filename pub-dir)
    ;; Handle Org errors at first.
    (user-error
     (message "%s" (error-message-string err)))
    ;; If the Org source is fine, handle LaTeX errors.
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (append-to-file (point-min) (point-max) "/dev/stdout")))))

;; Put table captions below the tables rather than above in LaTeX export.
;; However, if local publishing script defines a variable named
;; `custom-caption-placement' before loading this file, nothing will be changed
;; to the default table caption placement in LaTeX export.
(unless (boundp 'custom-caption-placement)
  (setq org-latex-caption-above nil))

(defun org-babel-execute-file (plist filename pub-dir)
  ;; A custom publishing function for executing all Babel source code blocks in
  ;; the Org file `filename'. Note that the function does not actually publish
  ;; anything to `pub-dir'.
  (progn
    (find-file filename)
    (org-babel-execute-buffer)
    (set-buffer-modified-p nil)
    (kill-buffer)))

(defun execute-block-in-file (block file)
  ;; Executes the named source code `block' in `file' without altering the
  ;; latter.
  (progn
    (find-file file)
    (org-babel-goto-named-src-block block)
    (org-babel-execute-src-block)
    (set-buffer-modified-p nil)
    (kill-buffer)))

;; Save global preferences for the appearance of code blocks.
(setq my-minted-options
      '(("linenos = false") ("mathescape") ("breaklines")
        ("bgcolor = yellow!15")))

;; Configure LaTeX to use 'minted' for code block export.
(setq org-latex-packages-alist '())
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options my-minted-options)

(defun set-minted-options-for-dark-background (&optional project-plist)
  ;; Adapt the options of the 'minted' package for themes using dark background
  ;; colors.
  (setq org-latex-minted-options
        '(("linenos = false") ("mathescape") ("breaklines")
          ("style = monokai"))))

(defun unset-minted-options-for-dark-background (&optional project-plist)
  ;; Reset default appearance settings for the 'minted' package.
  (setq org-latex-minted-options my-minted-options))

(defun disable-babel (&optional project-plist)
  ;; Disable Babel code evaluation.
  (setq org-export-babel-evaluate nil))

(defun enable-babel (&optional project-plist)
  ;; Enable Babel code evaluation.
  (setq org-export-babel-evaluate t))

(defun fix-svg (&optional project-plist)
  ;; If used, these functions shall be executed after exporting a particular
  ;; item of the project alist.
  (let
      ((images
        (directory-files
         (concat
          (plist-get project-plist :publishing-directory) "/figures/results")
         t "svg$")))
    (dolist (image images)
      (message "Applying 'svgfix' on `%s'..." image)
      (shell-command
       (concat "svgfix " image)))))

;; Include the Inria favicon in to the HTML header.
(setq org-html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
