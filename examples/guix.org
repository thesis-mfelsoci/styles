#+LaTeX_CLASS: beamer
#+LANGUAGE: en
#+OPTIONS: H:2 toc:nil author:nil email:nil date:nil ^:{}
#+BEAMER_FRAME_LEVEL: 1
#+TITLE: Guix theme example
#+LaTeX_HEADER: \author{
#+LaTeX_HEADER:   Marek Felšöci \\ [-1ex]
#+LaTeX_HEADER:   \texttt{marek.felsoci@inria.fr}
#+LaTeX_HEADER: }
#+BEAMER_HEADER: \usepackage{helvet}
#+BEAMER_HEADER: \usepackage[orgmode, inria]{styles/guix/beamerthemeguix}
#+BEAMER_HEADER: \institute{Inria Bordeaux Sud-Ouest}
#+BEAMER_HEADER: \date[April 29, 2022]{
#+BEAMER_HEADER:   Guix theme example \\
#+BEAMER_HEADER:   Fromulary 32, 3022
#+BEAMER_HEADER: }

#+INCLUDE: "./lorem.org" :lines "-14"
