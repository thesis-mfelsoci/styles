# Ph.D. thesis of Marek Felšöci: styles

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/styles/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/styles/-/commits/master)

This repository contains LaTeX styles, classes and themes used in the documents
related to the Ph.D. thesis of Marek Felšöci.

**[Browse examples](https://thesis-mfelsoci.gitlabpages.inria.fr/styles)**
